'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Product.belongsTo(models.User,{
        foreignKey:{
          onDelete: 'cascade',
          allowNull: false
        }
      })

      models.Product.hasOne(models.ExpirationAlert)
      models.Product.hasOne(models.ProductAlert)

    }
  }
  Product.init({
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    quantity: DataTypes.FLOAT,
    gencode: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};