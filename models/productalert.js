'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductAlert extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.ProductAlert.belongsTo(models.Product,{
        foreignKey:{
          onDelete: 'cascade',
          allowNull: false
        }
      })
    }
  }
  ProductAlert.init({
    limit_quantity: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductAlert',
  });
  return ProductAlert;
};