'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ExpirationAlert extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.ExpirationAlert.belongsTo(models.Product,{
        foreignKey:{
          onDelete: 'cascade',
          allowNull: false
        }
      })
    }
  }
  ExpirationAlert.init({
    expiration_alert: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'ExpirationAlert',
  });
  return ExpirationAlert;
};