let cors = require('cors')

let corsOptions = {
    origin: '*',
    optionsSuccessStatus: 204 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

module.exports = cors()