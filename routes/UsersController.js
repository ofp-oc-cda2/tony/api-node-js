// Import
let bcrypt = require('bcrypt')
let jwtUtils = require('../utils/jwt.util')
let models = require('../models')

// Constants
const EMAIL_REGEX     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEX  = /^(?=.*\d).{8,16}$/

// Routes
module.exports = {
    // Route Register
    register: function(req, res){
        //params
        let email = req.body.email
        let password = req.body.password

        // Verification
        if(email == null || password == null ){
            return res.status(400).json({'error' : 'missing parameters'})
        }

        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({ 'error': 'email is not valid' })
        }

        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({ 'error': 'password invalid (must length 8 - 16 and include 1 number at least)' })
        }


        // User
        models.User.findOne({
            attributes: ['email'],
            where: {email: email}
        })
        .then(function(userFound){
            if(!userFound){
                bcrypt.hash(password, 5)
                .then((bcryptedPassword) =>{
                    models.User.create({
                        email: email,
                        password: bcryptedPassword,
                        premium: 0,
                        isAdmin: 0
                    })
                    .then(newUser=>{
                        console.log(newUser)
                        return res.status(201).json({
                            'userId': newUser.id
                        })
                    })
                    .catch(function (err){
                        console.log(err)
                        return res.status(500).json({'error' : 'cannot add user'})
                    })
                })
                .catch(function (err){
                    console.log(err)
                    return res.status(500).json({'error' : 'cannot add user'})
                })
            }
            else{
                return res.status(409).json({'error' : 'user already exist'})
            }

        })
        .catch(function(err){
            console.log(err)
            return res.status(500).json({'error' : 'unable to verify user', err})
        })
    
    },
    // Route login
    login: function(req, res){
        //params
        let email = req.body.email
        let password = req.body.password

        if(email == null || password == null ){
            return res.status(400).json({'error' : 'missing parameters'})
        }

        models.User.findOne({
            where: {email: email}
        })
        .then(function(userFound){
            if(userFound){
                bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt){
                    if(resBycrypt){
                        return res.status(200).json({
                            'userId': userFound.id,
                            'token': jwtUtils.generateTokenForUser(userFound)
                        })
                    }
                    else{
                        return res.status(404).json({'error' : 'user doesn\'t exist in db'})
                    }
                })
            }
            else{
                return res.status(404).json({'error': 'user doesn\'t exist in db'})
            }
        })
        .catch(function(err){
            return res.status(500).json({'error': 'unable to verify user', err})
        })
     }, 
     // Route Get Profile
     getUserProfile: function(req, res){
         // Getting auth header
         let headerAuth = req.headers['authorization']
         let userId = jwtUtils.getUserId(headerAuth)

         if(userId < 0)
            return res.status(400).json({'error': 'wrong token'})

        models.User.findOne({
            attributes: ['id', 'email'],
            where: {id: userId}
        })
        .then(function(user){
            if (user){
                res.status(201).json(user)
            }
            else{
                res.status(404).json({'error': 'user not found'})
            }
        })
        .catch(function(err){
            res.status(500).json({'error': 'cannot fetch user'})
        })
     },
}