// Imports
let models   = require('../models')
let asyncLib = require('async')

// Constants

// Routes

module.exports = {
    AddProductAlert: function (req, res) {
        // Params
        let limit_quantity = req.body.limit_quantity
        let productId = req.body.productId

        if (limit_quantity == null) {
            return res.status(400).json({ 'error': 'missing parameters' })
        }

        asyncLib.waterfall([
            function (done) {
                models.Product.findOne({
                    where: { id: productId }
                })
                    .then(function (productFound) {
                        done(null, productFound)
                    })
                    .catch(function (err) {
                        console.log(err)
                        return res.status(500).json({ 'error': 'unable to verify product' })
                    })
            },
            function (productFound, done) {
                if (productFound) {
                    models.ProductAlert.create({
                        limit_quantity: limit_quantity,
                        ProductId: productFound.id
                    })
                        .then(function (newProductAlert) {
                            done(newProductAlert)
                        })
                } else {
                    res.status(404).json({ 'error': 'product not found' })
                }
            }
        ], function (newProductAlert) {
            if (newProductAlert) {
                return res.status(201).json(newProductAlert)
            }
            else {
                return res.status(500).json({ 'error': 'cannot add product alert' })
            }
        })
    },
    ListProductAlert: function (req, res) {
        let fields = req.query.fields
        let limit  = parseInt(req.query.limit)
        let offset = parseInt(req.query.offset)
        let order  = req.query.order

        models.ProductAlert.findAll({
            order: [(order != null) ? order.split(':') : ['id', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            limit: (!isNaN(limit)) ? limit : null,
            offset: (!isNaN(offset)) ? offset : null,
            include: [{
                model: models.Product,
                attributes: ['name']
            }]
        }).then(function (productalerts) {
            if (productalerts) {
                res.status(200).json(productalerts)
            }
            else {
                res.status(404).json({ 'error': 'no products alert found' })
            }
        }).catch(function (err) {
            console.log(err)
            res.status(500).json({ 'error': 'invalid field' })
        })
    },
    EditProductAlert: function (req, res) {
        // Params
        let id = req.params.id
        
        models.ProductAlert.update(req.body, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Product alert was updated successfully."
                    });
                } else {
                    res.send({
                        message: `Cannot update Product alert with id=${id}. Maybe Product was not found or req.body is empty!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating Product alert with id=" + id
                });
            });
    },
    DeleteProductAlert: function (req, res) {
        let id = req.params.id
        models.ProductAlert.destroy({
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Product alert was deleted successfully!"
                    });
                } else {
                    res.send({
                        message: `Cannot delete product alert with id=${id}. Maybe product was not found!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Could not delete product alert with id=" + id
                });
            });
    }
}