// Imports
let models = require("../models");
let jwtUtils = require("../utils/jwt.util");
let asyncLib = require("async");

// Constants

// Routes

module.exports = {
  AddProducts: function (req, res) {
    // Getting auth header
    let headerAuth = req.headers["authorization"];
    let userId = jwtUtils.getUserId(headerAuth);
    // Params
    let name = req.body.name;
    let type = req.body.type;
    let quantity = req.body.quantity;
    let limit_quantity = req.body.limit_quantity;
    let expiration_alert = req.body.expiration_alert;
    // let gencode = req.body.gencode

    if (name == null || type == null || quantity == null) {
      return res.status(400).json({ error: "missing parameters" });
    }

    // if (gencode.length != 13) {
    //     return res.status(400).json({ 'error': 'wrong gencode length' })
    // }

    asyncLib.waterfall(
      [
        function (done) {
          models.User.findOne({
            where: { id: userId },
          })
            .then(function (userFound) {
              done(null, userFound);
            })
            .catch(function (err) {
              return res.status(500).json({ error: "unable to verify user" });
            });
        },
        function (userFound, done) {
          if (userFound) {
            models.Product.create({
              name: name,
              type: type,
              quantity: quantity,
              UserId: userFound.id,
            }).then(function (newProduct) {
              models.ProductAlert.create({
                limit_quantity: limit_quantity,
                ProductId: newProduct.id,
              })
                .then(function (newProductAlert) {
                  return models.ExpirationAlert.create({
                    expiration_alert: expiration_alert,
                    ProductId: newProduct.id,
                  });
                })
                .then(function (newExpirationAlert) {
                  done(newProduct);
                });
            });
          } else {
            res.status(404).json({ error: "user not found" });
          }
        },
      ],
      function (newProduct) {
        if (newProduct) {
          return res.status(201).json(newProduct);
        } else {
          return res.status(500).json({ error: "cannot add product" });
        }
      }
    );
  },
  ListProducts: function (req, res) {
    let fields = req.query.fields;
    let limit = parseInt(req.query.limit);
    let offset = parseInt(req.query.offset);
    let order = req.query.order;

    console.log(req.userId)
    models.Product.findAll({
      order: [order != null ? order.split(":") : ["id", "ASC"]],
      attributes: fields !== "*" && fields != null ? fields.split(",") : null,
      where: {
        UserId: req.userId
      },
      limit: !isNaN(limit) ? limit : null,
      offset: !isNaN(offset) ? offset : null,
      include: [
        {
          model: models.User,
          attributes: ["email"],
        },
        {
          model: models.ExpirationAlert,
          attributes: ["expiration_alert"],
        },
        {
          model: models.ProductAlert,
          attributes: ["limit_quantity"]
        }
      ],
    })
      .then(function (products) {
        if (products) {
          res.status(200).json(products);
        } else {
          res.status(404).json({ error: "no products found" });
        }
      })
      .catch(function (err) {
        console.log(err);
        res.status(500).json({ error: "invalid field" });
      });
  },
  FindProduct: function (req, res) {
    const id = req.params.id;
    models.Product.findByPk(id, {
      include: [
        {
          model: models.User,
          attributes: ["email"],
        },
        {
          model: models.ExpirationAlert,
          attributes: ["expiration_alert"],
        },
        {
          model: models.ProductAlert,
          attributes: ["limit_quantity"]
        }
      ],
    })
      .then((data) => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Product with id=${id}.`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error retrieving Product with id=" + id,
        });
      });
  },
  EditProducts: function (req, res) {
    // Params
    let id = req.params.id;

    models.Product.update(req.body, {
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          models.ExpirationAlert.update({expiration_alert: req.body.expiration_alert}, {
            where: {Productid: id },
          })
          models.ProductAlert.update({limit_quantity: req.body.limit_quantity}, {
            where: {Productid: id },
          })
          res.send({
            message: "Product was updated successfully.",
          });
        } else {
          res.send({
            message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error updating Product with id=" + id,
        });
      });
  },
  DeleteProducts: function (req, res) {
    let id = req.params.id;
    models.Product.destroy({
      where: { id: id },
    })
      .then((num) => {
        if (num == 1) {
          res.send({
            message: "Product was deleted successfully!",
          });
        } else {
          res.send({
            message: `Cannot delete product with id=${id}. Maybe product was not found!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Could not delete product with id=" + id,
        });
      });
  },
};
