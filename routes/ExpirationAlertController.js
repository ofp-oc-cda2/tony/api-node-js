// Imports
let models   = require('../models')
let asyncLib = require('async')

// Constants

// Routes

module.exports = {
    AddExpirationAlert: function (req, res) {
        // Params
        let expiration_alert = req.body.expiration_alert
        let productId = req.body.productId

        if (expiration_alert == null) {
            return res.status(400).json({ 'error': 'missing parameters' })
        }

        asyncLib.waterfall([
            function (done) {
                models.Product.findOne({
                    where: { id: productId }
                })
                    .then(function (productFound) {
                        done(null, productFound)
                    })
                    .catch(function (err) {
                        console.log(err)
                        return res.status(500).json({ 'error': 'unable to verify product' })
                    })
            },
            function (productFound, done) {
                if (productFound) {
                    models.ExpirationAlert.create({
                        expiration_alert: expiration_alert,
                        ProductId: productFound.id
                    })
                        .then(function (newExpirationAlert) {
                            done(newExpirationAlert)
                        })
                } else {
                    res.status(404).json({ 'error': 'product not found' })
                }
            }
        ], function (newExpirationAlert) {
            if (newExpirationAlert) {
                return res.status(201).json(newExpirationAlert)
            }
            else {
                return res.status(500).json({ 'error': 'cannot add expiration alert' })
            }
        })
    },
    ListExpirationAlert: function (req, res) {
        let fields = req.query.fields
        let limit  = parseInt(req.query.limit)
        let offset = parseInt(req.query.offset)
        let order  = req.query.order

        models.ExpirationAlert.findAll({
            order: [(order != null) ? order.split(':') : ['id', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            limit: (!isNaN(limit)) ? limit : null,
            offset: (!isNaN(offset)) ? offset : null,
            include: [{
                model: models.Product,
                attributes: ['name']
            }]
        }).then(function (ExpirationAlerts) {
            if (ExpirationAlerts) {
                res.status(200).json(ExpirationAlerts)
            }
            else {
                res.status(404).json({ 'error': 'no expiration alert found' })
            }
        }).catch(function (err) {
            console.log(err)
            res.status(500).json({ 'error': 'invalid field' })
        })
    },
    EditExpirationAlert: function (req, res) {
        // Params
        let id = req.params.id
        
        models.ExpirationAlert.update(req.body, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Expiration alert was updated successfully."
                    });
                } else {
                    res.send({
                        message: `Cannot update Expiration alert with id=${id}. Maybe Expiration was not found or req.body is empty!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating Expiration alert with id=" + id
                });
            });
    },
    DeleteExpirationAlert: function (req, res) {
        let id = req.params.id
        models.ExpirationAlert.destroy({
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "expiration alert was deleted successfully!"
                    });
                } else {
                    res.send({
                        message: `Cannot delete expiration alert with id=${id}. Maybe expiration was not found!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Could not delete expiration alert with id=" + id
                });
            });
    }
}