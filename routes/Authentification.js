const {getUserId} = require('../utils/jwt.util')



function authenticate(req, res, next) {

const userId = getUserId(req.headers['authorization'])
    if(userId === -1){
        res.status(403).send()
    }else{
        req.userId = userId
        next()
    }
}


module.exports = authenticate
