// Import
let express        = require('express')
let usersCtrl      = require('./routes/UsersController')
let productsCtrl   = require('./routes/ProductsController')
let stockCtrl      = require('./routes/ProductAlertController')
let expirationCtrl = require('./routes/ExpirationAlertController')
let authenticate   = require('./routes/Authentification')
// Router
exports.router = (function(){
    let apiRouter = express.Router()

    // Users routes
    apiRouter.route('/users/register').post(usersCtrl.register)
    apiRouter.route('/users/login').post(usersCtrl.login)
    apiRouter.route('/users/current').get(authenticate,usersCtrl.getUserProfile)
    // Products routes
    apiRouter.route('/products/new').post(productsCtrl.AddProducts)
    apiRouter.route('/products/:id').put(productsCtrl.EditProducts)
    apiRouter.route('/products/:id').delete(productsCtrl.DeleteProducts)
    apiRouter.route('/products').get(authenticate,productsCtrl.ListProducts)
    apiRouter.route('/products/:id').get(productsCtrl.FindProduct)
    // Expiration Products routes
    apiRouter.route('/expirationalert/new').post(expirationCtrl.AddExpirationAlert)
    apiRouter.route('/expirationalert/:id').put(expirationCtrl.EditExpirationAlert)
    apiRouter.route('/expirationalert/:id').delete(expirationCtrl.DeleteExpirationAlert)
    apiRouter.route('/expirationalert').get(expirationCtrl.ListExpirationAlert)
    // Stock Products routes
    apiRouter.route('/productsalert/new').post(stockCtrl.AddProductAlert)
    apiRouter.route('/productsalert/:id').put(stockCtrl.EditProductAlert)
    apiRouter.route('/productsalert/:id').delete(stockCtrl.DeleteProductAlert)
    apiRouter.route('/productsalert').get(stockCtrl.ListProductAlert)

    return apiRouter
})()